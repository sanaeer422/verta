<?php
/*
 * Copyright (C) 2017 beGateway
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @author      beGateway
 * @copyright   2017 beGateway
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU General Public License, version 2 (GPL-2.0)
 */

namespace BeGateway\BeGateway\Model\Method;

use BeGateway\BeGateway\Helper\Data;
use BeGateway\BeGateway\Model\Traits\OnlinePaymentMethod;
use BeGateway\GetPaymentToken;
use BeGateway\PaymentMethod\CreditCard;
use BeGateway\PaymentMethod\CreditCardHalva;
use BeGateway\PaymentMethod\Erip;
use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use stdClass;

/**
 * Checkout Payment Method Model Class
 * Class Checkout
 * @package BeGateway\BeGateway\Model\Method
 */
class Checkout extends AbstractMethod
{
    use OnlinePaymentMethod;

    const CODE = 'begateway_checkout';
    /**
     * Checkout Method Code
     */
    protected $_code = self::CODE;

    protected $_canOrder                    = true;
    protected $_isGateway                   = true;
    protected $_canCapture                  = true;
    protected $_canCapturePartial           = true;
    protected $_canRefund                   = true;
    protected $_canCancelInvoice            = true;
    protected $_canVoid                     = true;
    protected $_canRefundInvoicePartial     = true;
    protected $_canAuthorize                = true;
    protected $_isInitializeNeeded          = false;

    /**
     * Get Instance of the Magento Code Logger
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        return $this->_logger;
    }

    /**
     * Checkout constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param Context $actionContext
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param StoreManagerInterface $storeManager
     * @param Session $checkoutSession
     * @param Data $moduleHelper
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     * @throws LocalizedException
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        Context $actionContext,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger  $logger,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        Data $moduleHelper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
        $this->_actionContext = $actionContext;
        $this->_storeManager = $storeManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_moduleHelper = $moduleHelper;
        $this->_configHelper =
            $this->getModuleHelper()->getMethodConfig(
                $this->getCode()
            );
    }

    /**
     * Get Default Payment Action On Payment Complete Action
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return AbstractMethod::ACTION_ORDER;
    }

    /**
     * Get Available Checkout Transaction Types
     * @return array
     */
    public function getCheckoutTransactionTypes()
    {
        $selected_types = $this->getConfigHelper()->getTransactionTypes();

        return $selected_types;
    }

    /**
     * Create a Web-Payment Form Instance
     * @param array $data
     * @return stdClass
     * @throws \Magento\Framework\Webapi\Exception
     */
    protected function checkout($data)
    {
        $transaction = new GetPaymentToken();

        $transaction->money->setAmount($data['order']['amount']);
        $transaction->money->setCurrency($data['order']['currency']);
        $transaction->setDescription($data['order']['description']);
        $transaction->setTrackingId($data['tracking_id']);
        $transaction->setLanguage($data['order']['language']);
        $transaction->customer->setFirstName((string)$data['order']['billing']->getFirstname());
        $transaction->customer->setLastName((string)$data['order']['billing']->getLastname());
        $transaction->customer->setAddress((string)$data['order']['billing']->getStreetLine(1));
        $transaction->customer->setCity((string)$data['order']['billing']->getCity());
        $transaction->customer->setCountry((string)$data['order']['billing']->getCountryId());
        $transaction->customer->setZip($data['order']['billing']->getPostcode());
        $transaction->setTestMode((int)$this->getConfigHelper()->getTestMode() == 1);

        if (in_array((string)$data['order']['billing']->getCountryId(), ['US', 'CA'])) {
            $transaction->customer->setState((string)$data['order']['billing']->getRegionCode());
        }

        if (!empty((string)$data['order']['customer']['email'])) {
            $transaction->customer->setEmail((string)$data['order']['customer']['email']);
        }

        $transaction->customer->setPhone((string)$data['order']['billing']->getTelephone());

        $notification_url = $data['urls']['notify'];
        $notification_url = str_replace('carts.local', 'webhook.begateway.com:8443', $notification_url);
        $transaction->setNotificationUrl($notification_url);

        $transaction->setSuccessUrl($data['urls']['return_success']);
        $transaction->setDeclineUrl($data['urls']['return_failure']);
        $transaction->setFailUrl($data['urls']['return_failure']);
        $transaction->setCancelUrl($data['urls']['return_cancel']);

        $payment_methods = $this->getCheckoutTransactionTypes();
        $helper = $this->getModuleHelper();

        if (in_array($helper::CREDIT_CARD, $payment_methods)) {
            $cc = new CreditCard();
            $transaction->addPaymentMethod($cc);
        }

        if (in_array($helper::CREDIT_CARD_HALVA, $payment_methods)) {
            $halva = new CreditCardHalva();
            $transaction->addPaymentMethod($halva);
        }

        if (in_array($helper::ERIP, $payment_methods)) {
            $erip = new Erip([
          'order_id' => $data['order']['increment_id'],
          'account_number' => (string)$data['order']['increment_id'],
          'service_no' => $data['erip']['service_no'],
          'service_info' => [$data['erip']['service_info']]
        ]);
            $transaction->addPaymentMethod($erip);
        }

        $response = $transaction->submit();

        return $response;
    }

    /**
     * Order Payment
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws LocalizedException
     */
    public function order(InfoInterface $payment, $amount)
    {
        $order = $payment->getOrder();

        $orderId = ltrim(
            $order->getIncrementId(),
            '0'
        );

        $data = [
            'tracking_id' => $this->getModuleHelper()->genTransactionId(
                  $orderId
                ),
            'transaction_types' => $this->getConfigHelper()->getTransactionTypes(),
            'order' => [
                'increment_id' => $orderId,
                'currency' => $order->getBaseCurrencyCode(),
                'language' => $this->getModuleHelper()->getLocale(),
                'amount' => $amount,
                'usage' => $this->getModuleHelper()->buildOrderUsage(),
                'description' => __('Order # %1 payment', $orderId),
                'customer' => [
                    'email' => $this->getCheckoutSession()->getQuote()->getCustomerEmail(),
                ],
                'billing' => $order->getBillingAddress(),
                'shipping' => $order->getShippingAddress()
            ],
            'erip' => [
              'service_no' => $this->getConfigHelper()->getValue('erip_service_no'),
              'service_info' => [
                __('Order # %1 payment', $orderId),
                $this->getModuleHelper()->buildOrderDescriptionText(
                  $order
                ),
              ],
            ],
            'urls' => [
                'notify' => $this->getModuleHelper()->getNotificationUrl(
                        $this->getCode()
                    ),
                'return_success' => $this->getModuleHelper()->getReturnUrl(
                        $this->getCode(),
                        'success'
                    ),
                'return_cancel'  => $this->getModuleHelper()->getReturnUrl(
                        $this->getCode(),
                        'cancel'
                    ),
                'return_failure' => $this->getModuleHelper()->getReturnUrl(
                        $this->getCode(),
                        'failure'
                    ),
            ]
        ];

        $this->getConfigHelper()->initGatewayClient();

        try {
            $responseObject = $this->checkout($data);

            $isBeGatewaySuccessful =
                $responseObject->isSuccess() && !empty($responseObject->getRedirectUrl());

            if (!$isBeGatewaySuccessful) {
                $errorMessage = $responseObject->getMessage();

                $this->getCheckoutSession()->setBeGatewayLastCheckoutError(
                    $errorMessage
                );

                $this->getModuleHelper()->throwWebApiException($errorMessage);
            }

            $payment->setTransactionId($responseObject->getToken());
            $payment->setIsTransactionPending(true);
            $payment->setIsTransactionClosed(false);

            $this->getModuleHelper()->setPaymentTransactionAdditionalInfo(
                $payment,
                $responseObject
            );

            $this->getCheckoutSession()->setBeGatewayCheckoutRedirectUrl(
                $responseObject->getRedirectUrl()
            );

            return $this;
        } catch (Exception $e) {
            $this->getLogger()->error(
                $e->getMessage()
            );

            $this->getCheckoutSession()->setBeGatewayLastCheckoutError(
                $e->getMessage()
            );

            $this->getModuleHelper()->maskException($e);
        }
    }

    /**
     * Payment Capturing
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function capture(InfoInterface $payment, $amount)
    {
        /** @var Order $order */
        $order = $payment->getOrder();

        $this->getLogger()->debug('Capture transaction for order #' . $order->getIncrementId());

        $authTransaction = $this->getModuleHelper()->lookUpAuthorizationTransaction(
            $payment
        );

        if (!isset($authTransaction)) {
            $errorMessage = __(
                'Capture transaction for order # %1 cannot be finished (No Authorize Transaction exists)',
                $order->getIncrementId()
            );

            $this->getLogger()->error(
                $errorMessage
            );

            $this->getModuleHelper()->throwWebApiException(
                $errorMessage
            );
        }

        try {
            $this->doCapture($payment, $amount, $authTransaction);
        } catch (Exception $e) {
            $this->getLogger()->error(
                $e->getMessage()
            );
            $this->getModuleHelper()->maskException($e);
        }

        return $this;
    }

    /**
     * Payment refund
     *
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function refund(InfoInterface $payment, $amount)
    {
        /** @var Order $order */
        $order = $payment->getOrder();

        $this->getLogger()->debug('Refund transaction for order #' . $order->getIncrementId());

        $captureTransaction = $this->getModuleHelper()->lookUpCaptureTransaction(
            $payment
        );

        if (!isset($captureTransaction)) {
            $errorMessage = __(
                'Refund transaction for order # %1 cannot be finished (No Capture Transaction exists)',
                $order->getIncrementId()
            );

            $this->getLogger()->error(
                $errorMessage
            );

            $this->getMessageManager()->addError($errorMessage);

            $this->getModuleHelper()->throwWebApiException(
                $errorMessage
            );
        }

        try {
            $this->doRefund($payment, $amount, $captureTransaction);
        } catch (Exception $e) {
            $this->getLogger()->error(
                $e->getMessage()
            );

            $this->getMessageManager()->addError(
                $e->getMessage()
            );

            $this->getModuleHelper()->maskException($e);
        }

        return $this;
    }

    /**
     * Payment Cancel
     * @param InfoInterface $payment
     * @return $this
     */
    public function cancel(InfoInterface $payment)
    {
        $this->void($payment);

        return $this;
    }

    /**
     * Void Payment
     * @param InfoInterface $payment
     * @return $this
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function void(InfoInterface $payment)
    {
        /** @var Order $order */

        $order = $payment->getOrder();

        $this->getLogger()->debug('Void transaction for order #' . $order->getIncrementId());

        $referenceTransaction = $this->getModuleHelper()->lookUpVoidReferenceTransaction(
            $payment
        );

        if ($referenceTransaction->getTxnType() == Transaction::TYPE_AUTH) {
            $authTransaction = $referenceTransaction;
        } else {
            $authTransaction = $this->getModuleHelper()->lookUpAuthorizationTransaction(
                $payment
            );
        }

        if (!isset($authTransaction) || !isset($referenceTransaction)) {
            $errorMessage = __(
                'Void transaction for order # %1 cannot be finished (No Authorize / Capture Transaction exists)',
                            $order->getIncrementId()
            );

            $this->getLogger()->error($errorMessage);
            $this->getModuleHelper()->throwWebApiException($errorMessage);
        }

        try {
            $this->doVoid($payment, $authTransaction, $referenceTransaction);
        } catch (Exception $e) {
            $this->getLogger()->error(
                $e->getMessage()
            );
            $this->getModuleHelper()->maskException($e);
        }

        return $this;
    }

    /**
     * Determines method's availability based on config data and quote amount
     *
     * @param CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(CartInterface $quote = null)
    {
        return parent::isAvailable($quote) &&
            $this->getConfigHelper()->isMethodAvailable();
    }

    /**
     * Checks base currency against the allowed currency
     *
     * @param string $currencyCode
     * @return bool
     * @throws LocalizedException
     */
    public function canUseForCurrency($currencyCode)
    {
        return $this->getModuleHelper()->isCurrencyAllowed(
            $this->getCode(),
            $currencyCode
        );
    }
}
