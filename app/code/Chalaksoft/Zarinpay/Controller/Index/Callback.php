<?php

namespace Chalaksoft\Zarinpay\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;

//require_once(__DIR__.'/nusoap.php');

class Callback extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
    protected $_resultPageFactory;

    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {

       /* $RefNum=$_POST['RefNum'];
        $State=$_POST['State'];
        $ResNum=$_POST['ResNum'];

        if($State=='OK')
        {
            $Verify_URL='https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL';

            $soapclient = new \nusoap_client($Verify_URL,'wsdl');
            $soapProxy = $soapclient->getProxy() ;
            $res=  $soapProxy->verifyTransaction($RefNum,"21308383");#reference number and sellerid
            if( $res <= 0 )
            {
                echo '<h1>نتیجه پرداخت چوبینجا<h1>';
                echo '<h3>ظاهرا خطایی رخ داده است نگران نباشید و حسابتون را چک کنید و نهایتا با فروشگاه تماس بگیرید<h3>' ;
                echo '<h5>Amount:'.$res;
                echo '<h5>RefNum:'.$RefNum;
                echo '<h5>ResNum:'.$_POST['ResNum'];
                $this->messageManager->addSuccessMessage('پرداخت با موفقیت انجام شد');
                return $this->resultRedirectFactory->create()->setPath('signup/index/new');    }
            else
            {
                echo '<h1>نتیجه پرداخت چوبینجا<h1>';
                echo '<h3>پرداخت انجام شد... درحال انتقال به سایت پذیرنده...لطفا صبر نمایید</h3>';
                echo '<h5>Amount:'.$res;
                echo '<h5>RefNum:'.$RefNum;
                echo '<h5>ResNum:'.$_POST['ResNum'];
            }
            ?>
            <!--<form name="form1" action="http://choobinja.com/zarinpay/index/new" method="POST">
    	<input type="hidden" name="token" value="<?php echo '' ?>">
    	<input type="hidden" name="verified" value="<?php echo $res; ?>">
    	<input type="hidden" name="amount" value="<?php echo $res; ?>">
    	<input type="hidden" name="referenceId" value="<?php echo $RefNum; ?>">
    	<input type="hidden" name="paymentId" value="<?php echo $ResNum; ?>">
    	<input type="hidden" name="invoiceNo" value="<?php echo '0'; ?>">
    	<input type="hidden" name="cardNo" value="<?php echo 'Saman'; ?>">
    </form>
    <script>document.form1.submit()</script>-->
            <?php
        }
        else
            echo 'STATE:'.$State;*/


        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set("بازگشت از درگاه پرداخت");
        return $resultPage;
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}