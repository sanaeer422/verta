<?php

namespace Chalaksoft\Zarinpay\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    const API_URL      = 'https://pec.shaparak.ir/NewIPGServices/Sale/SaleService.asmx?wsdl';
    const API_USERNAME = 'checklicense';
    const API_PASSWORD = 'n2w3z2y0kc';

    protected $_resultPageFactory;

    public function __construct(Context $context, ResultFactory $resultFactory)
    {
        $this->resultFactory = $resultFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        echo "salam";
//         $resultPage = $this->_resultPageFactory->create();
//        $resultPage->getConfig()->getTitle()->set("  درگاه پرداخت");
//
//        return $resultPage;

//        $this->getResponse()->setRedirect('https://sep.shaparak.ir/Payment.aspx');
//        $resultRedirect = $this->resultRedirectFactory->create();
//        $resultRedirect->setPath('https://sep.shaparak.ir/Payment.aspx', ['params' => $params]);

//        $params = ['MID' => '21308383'];
//        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
//        $resultRedirect->setPath('https://sep.shaparak.ir/Payment.aspx', ['params' => $params]);
//        $this->getRequest()->setPosts('order_ids', $orderIds);
//        return $resultRedirect;

//        $order_id = $this->getCheckoutSession()->getLastRealOrderId();
//        $order = $this->getOrderFactory()->create()->loadByIncrementId($order_id);
//        $this->getCheckoutSession()->setLastSuccessQuoteId($order->getQouteId());
//        $this->getCheckoutSession()->setLastQuoteId($order->getQouteId());
//        $this->getCheckoutSession()->setLastOrderId($order->getEntityId());
//        $result_redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
//        $result_redirect->setUrl($this->getStoreManager()->getStore()->getBaseUrl());
//        return $result_redirect;

//        $this->getResponse()->setRedirect('https://sep.shaparak.ir/Payment.aspx');

        if (!extension_loaded('soap')) {
            throw new \Magento\Framework\Webapi\Exception(
                __('SOAP extension is not loaded.'),
                0,
                \Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR
            );
        }

        try {
            $opts = [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false
                ]
            ];
            $context = stream_context_create($opts);
            $params = ['soap_version'=>SOAP_1_2,
                            'verifypeer' => false,
                            'verifyhost' => false,
                            'exceptions' => 1,
                            'stream_context'=>$context];

            $proxy = new \SoapClient(self::API_URL, $params);

            $pin = '1234325';
            $now = time();
            $rand = mt_rand(12345678, 98765432);
            $callback = "http://magehero.lo/verify.php?rand={$rand}&time={$now}";
            $price = 20000;// RIAL

            $params1 = [
                'LoginAccount' => $pin ,
                'Amount' => $price ,
                'OrderId' => $now,
                'CallBackUrl' => $callback,
                'authority' => 0,
                'status' => 1 ,
            ];
            $sendParams = ['requestData'=>$params1];
            //فرستادن مقادیر
            $res = $proxy->SalePaymentRequest($sendParams);
            print_r($res);

        } catch (\SoapFault $e) {
            echo $e->getMessage();
        }
    }
}
